import sys
import jinja2
import datetime
from   os                     import walk
from   os                     import path
from   astral                 import LocationInfo
from   calendar               import monthrange
from   astral.sun             import sun
from   dateutil.relativedelta import relativedelta

loc = LocationInfo(timezone='Europe/Berlin', latitude=52.4937522, longitude=13.4481537)
iconpath = "https://icons.23-5.eu"

year  = datetime.date.today().year
month = datetime.date.today().month
day   = datetime.date.today().day 

focus    = None

if len(sys.argv) < 2:
    print("Missing parameter must be d/m year month [day]")
    sys.exit()

if sys.argv[1] == "m":
    focus = 0
elif sys.argv[1] == "d":
    focus = 1
else:
    print("Error first parameter must be m or d")
    sys.exit()

if sys.argv[2] == "x":
    y     = datetime.date.today() - relativedelta(days = 1)
    year  = y.year
    month = y.month
    day   = y.day
else:
    year  = int(sys.argv[2])
    month = int(sys.argv[3])
    day   = int(sys.argv[4])

month_name = {}
month_name[ 1] = "January"
month_name[ 2] = "February"
month_name[ 3] = "March"
month_name[ 4] = "April"
month_name[ 5] = "May"
month_name[ 6] = "June"
month_name[ 7] = "July"
month_name[ 8] = "August"
month_name[ 9] = "September"
month_name[10] = "October"
month_name[11] = "November"
month_name[12] = "December"

suntoday    = sun(loc.observer, date= datetime.date(year, month, day), tzinfo=loc.timezone)
suntomorrow = sun(loc.observer, date=(datetime.date(year, month, day) + relativedelta(days=1)), tzinfo=loc.timezone)

vars = {}
vars['title_date'] = "%s - %s - %s" % (year, month, day)
vars['year']       = year
vars['month']      = month
vars['month_name'] = month_name[month]
vars['day']        = day
vars['sunset']     = suntoday['sunset'] 
vars['sunrise']    = suntomorrow['sunrise'] 
vars['iconpath']   = iconpath

today_dt = datetime.datetime(year, month, day, 12, 12, 12)

links = {}
links['day_to_last_year']     = (today_dt - relativedelta(years  = 1)).strftime("../%Y%m%d/index.html")
links['day_to_next_year']     = (today_dt + relativedelta(years  = 1)).strftime("../%Y%m%d/index.html")
links['day_to_last_month']    = (today_dt - relativedelta(months = 1)).strftime("../%Y%m%d/index.html")
links['day_to_next_month']    = (today_dt + relativedelta(months = 1)).strftime("../%Y%m%d/index.html")
links['day_to_last_day']      = (today_dt - relativedelta(days   = 1)).strftime("../%Y%m%d/index.html")
links['day_to_next_day']      = (today_dt + relativedelta(days   = 1)).strftime("../%Y%m%d/index.html")
links['day_to_current_month'] = (today_dt                            ).strftime("../%Y%m.html")

links['month_to_last_year']   = (today_dt - relativedelta(years  = 1)).strftime("%Y%m.html")
links['month_to_next_year']   = (today_dt + relativedelta(years  = 1)).strftime("%Y%m.html")
links['month_to_last_month']  = (today_dt - relativedelta(months = 1)).strftime("%Y%m.html")
links['month_to_next_month']  = (today_dt + relativedelta(months = 1)).strftime("%Y%m.html")

firstmonth, lastmonth = monthrange(year, month)

monthinfo = []
for d in range(1, lastmonth + 1):
    data            = {}
    data['number']  = d
    data['daylink'] = "%d%02d%02d" % (year, month, d)
    data['sunset']  = sun(loc.observer, date= datetime.date(year, month, d), tzinfo=loc.timezone)['sunset'] 
    data['sunrise'] = sun(loc.observer, date=(datetime.date(year, month, d) + relativedelta(days=1)), tzinfo=loc.timezone)['sunrise'] 
    
    if path.exists("/host/images/%d%02d%02d" % (year,month,d)):
        monthinfo.append(data) 

thumps = []
for (dirpath, dirnames, filenames) in walk(today_dt.strftime("/host/images/%Y%m%d/original")):
    thumps.extend(filenames)
    break

templateLoader = jinja2.FileSystemLoader(searchpath="/app/templates")
templateEnv    = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE  = None

if focus == 0:
    TEMPLATE_FILE  = "month.html"
elif focus == 1:
    TEMPLATE_FILE  = "daily.html"
else:
    sys.exit

template       = templateEnv.get_template(TEMPLATE_FILE)
outputText     = template.render(vars=vars, links=links, thumps=thumps, monthinfo=monthinfo)

if focus == 0:
    with open(today_dt.strftime("/host/images/%Y%m.html"), 'w') as the_file:
        the_file.write(outputText)
elif focus == 1:
    with open(today_dt.strftime("/host/images/%Y%m%d/index.html"), 'w') as the_file:
        the_file.write(outputText)
else:
    sys.exit
