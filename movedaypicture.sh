#!/bin/bash

HP="/host"
DATEPATTERN=`date -d "yesterday 13:00" '+%Y%m%d'`
IMAGEPATTERN="image-${DATEPATTERN}*.jpg"
FOLDERPATTERN=${HP}/daytime/${DATEPATTERN}

mkdir -p ${FOLDERPATTERN}
mv ${HP}/tmp/${IMAGEPATTERN} /${FOLDERPATTERN}
