import os
import sys
import time
import redis
import serial
import datetime
import traceback

redis_client    = redis.StrictRedis(host='localhost', port=6379, db=0)

def getValues(ser):
  line = ser.readline().decode("UTF-8")

  if len(line) < 2:
    return

  if line.startswith("INFO=>"):
      print(line)

  if line.startswith("ERROR=>"):
      print(line)

  if line.startswith("DATA=>"):
    elements = line.strip()[6:].split(',')
    redis_client.setex("/allsky/temp/air",       datetime.timedelta(minutes=1), value=float(elements[0]))
    redis_client.setex("/allsky/temp/dome",      datetime.timedelta(minutes=1), value=float(elements[1]))
    redis_client.setex("/allsky/temp/heating",   datetime.timedelta(minutes=1), value=float(elements[2]))
    redis_client.setex("/allsky/light/uv",       datetime.timedelta(minutes=1), value=float(elements[3]))
    redis_client.setex("/allsky/light/visible",  datetime.timedelta(minutes=1), value=  int(elements[4]))
    redis_client.setex("/allsky/sound/min",      datetime.timedelta(minutes=1), value=float(elements[5]))
    redis_client.setex("/allsky/sound/max",      datetime.timedelta(minutes=1), value=float(elements[6]))

  if line.startswith("STATE=>"):
    elements = line.strip()[7:].split(',')
    redis_client.setex("/state/heating",  datetime.timedelta(minutes=1), value=int(elements[0]))
    redis_client.setex("/state/motion",   datetime.timedelta(minutes=1), value=int(elements[1]))

  if line.startswith("SETTINGS=>"):
    elements = line.strip()[10:].split(',')
    redis_client.setex("/settings/temperature", datetime.timedelta(minutes=1), value=float(elements[0]))

  if redis_client.exists("/set/temperature"):
    ser.write('T{:02d}X'.format(int(redis_client.get("/set/temperature"))).encode())
    redis_client.delete("/set/temperature")

while True:

  try:
    with serial.Serial('/dev/ttyUSB0', 9600, timeout=10) as ser:
      while True:
        getValues(ser)

  except KeyboardInterrupt:
    sys.exit(1)

  except:
    traceback.print_exc()
    time.sleep(5)
