#ifndef ALLSKY_SOUND_H
#define ALLSKY_SOUND_H

extern float allsky_sound_min;
extern float allsky_sound_max;

#define ALLSKY_SOUND_SAMPLE_SIZE 23

void allsky_sound_init  (void);
void allsky_sound_update(void);
void allsky_sound_reset (void);
#endif
