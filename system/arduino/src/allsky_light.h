#ifndef ALLSKY_LIGHT_H
#define ALLSKY_LIGHT_H

extern float    allsky_light_uv;
extern uint32_t allsky_light_visible;

#define ALLSKY_LIGHT_SAMPLE_SIZE 23

void allsky_light_init   (void);
void allsky_light_update (void);

#endif
