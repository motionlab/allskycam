#ifndef ALLSKY_MOTION_H
#define ALLSKY_MOTION_H

extern bool allsky_motion_state;

void allsky_motion_init  (void);
void allsky_motion_update(void);

#endif
