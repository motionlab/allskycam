#include <Arduino.h>
#include "allsky_uplink.h"
#include "allsky_dallas.h"
#include "allsky_heating.h"
#include "allsky_light.h"
#include "allsky_motion.h"
#include "allsky_sound.h"

uint8_t allsky_uplink_receive_buffer[ALLSKY_UPLINK_RECEIVE_BUFFER_SIZE] = {0};
uint8_t allsky_uplink_receive_index                                     = 0;

void allsky_uplink_init(void){}

void allsky_uplink_send(void){
    Serial.print("DATA=>");
    Serial.print(allsky_dallas_temp_air);
    Serial.print(",");
    Serial.print(allsky_dallas_temp_dome);
    Serial.print(",");
    Serial.print(allsky_dallas_temp_heating);
    Serial.print(",");
    Serial.print(allsky_light_uv);
    Serial.print(",");
    Serial.print(allsky_light_visible);
    Serial.print(",");
    Serial.print(allsky_sound_min);
    Serial.print(",");
    Serial.print(allsky_sound_max);
    Serial.println("");

    Serial.print("STATE=>");
    Serial.print(allsky_heating_state);
    Serial.print(",");
    Serial.print(allsky_motion_state);
    Serial.println("");

    Serial.print("SETTINGS=>");
    Serial.print(allsky_heating_temperature);
    Serial.println("");

    allsky_sound_reset();
}

void allsky_uplink_receive(void){

    if(Serial.available() > 0){
        allsky_uplink_receive_buffer[allsky_uplink_receive_index++] = Serial.read();
    }

    if(allsky_uplink_receive_index > ALLSKY_UPLINK_RECEIVE_BUFFER_SIZE){
        Serial.println("ERROR=>Receive buffer overflow");
        memset(allsky_uplink_receive_buffer, 0, ALLSKY_UPLINK_RECEIVE_BUFFER_SIZE);
        allsky_uplink_receive_index = 0;
    }

    if(allsky_uplink_receive_index > 1 && allsky_uplink_receive_buffer[allsky_uplink_receive_index - 1] == 'X'){
        String s = (char *)allsky_uplink_receive_buffer;

        if (s.startsWith("T")){
            uint8_t temp = s.substring(1, 3).toInt();

            if(temp >= ALLSKY_UPLINK_MIN_TEMP && temp <= ALLSKY_UPLINK_MAX_TEMP && temp >= ALLSKY_UPLINK_MIN_TEMP){
                Serial.print("INFO=>OK, temp set to ");
                Serial.println(temp);
                allsky_heating_temperature = temp;
            } else {
                Serial.println("ERROR=>Wrong temp value");
            }
        }

        allsky_uplink_receive_index = 0;
        memset(allsky_uplink_receive_buffer, 0, ALLSKY_UPLINK_RECEIVE_BUFFER_SIZE);
    }
}
