#ifndef ALLSKY_PINOUT_H
#define ALLSKY_PINOUT_H

#define ALLSKY_PINOUT_RX                0
#define ALLSKY_PINOUT_TX                1

#define ALLSKY_PINOUT_MOTION            2 // Interrupt
#define ALLSKY_PINOUT_03                3 // Interrupt PWM
#define ALLSKY_PINOUT_DALLAS_BUS        4
#define ALLSKY_PINOUT_05                5 // PWM
#define ALLSKY_PINOUT_06                6 // PWM
#define ALLSKY_PINOUT_07                7
#define ALLSKY_PINOUT_HEATING           8
#define ALLSKY_PINOUT_09                9 // PWM

#define ALLSKY_PINOUT_SS               10 // PWM
#define ALLSKY_PINOUT_MOSI             11 // PWM
#define ALLSKY_PINOUT_MISO             12
#define ALLSKY_PINOUT_SCK              13 // Also Build in LED

#define ALLSKY_PINOUT_LIGHT_UV         A0
#define ALLSKY_PINOUT_SOUND            A1
#define ALLSKY_PINOUT_A2               A2
#define ALLSKY_PINOUT_A3               A3
#define ALLSKY_PINOUT_SDA              A4
#define ALLSKY_PINOUT_SCL              A5
#define ALLSKY_PINOUT_A6               A6
#define ALLSKY_PINOUT_A7               A7

#endif
