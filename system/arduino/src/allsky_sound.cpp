#include <Arduino.h>
#include <float.h>
#include "allsky_sound.h"
#include "allsky_pinout.h"

float allsky_sound_min = FLT_MAX;
float allsky_sound_max = 0.0;

void allsky_sound_init(void){
    pinMode(ALLSKY_PINOUT_SOUND, INPUT);
}

void allsky_sound_update(void){
    double temp = 0;

    for (uint8_t i = 0; i < ALLSKY_SOUND_SAMPLE_SIZE; i++){
        temp += analogRead(ALLSKY_PINOUT_SOUND);
    }

    allsky_sound_min = min(allsky_sound_min, temp);
    allsky_sound_max = max(allsky_sound_max, temp);
}

void allsky_sound_reset(void){
    allsky_sound_min = FLT_MAX;
    allsky_sound_max = 0.0;
}
