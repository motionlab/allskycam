#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "allsky_dallas.h"
#include "allsky_pinout.h"

float allsky_dallas_temp_air     = 0;
float allsky_dallas_temp_dome    = 0;
float allsky_dallas_temp_heating = 0;

DeviceAddress dome_sensor   = { 0x28, 0xFF, 0x8A, 0x00, 0x33, 0x17, 0x03, 0x64 };
DeviceAddress air_sensor    = { 0x28, 0xFF, 0xB0, 0x8B, 0x33, 0x17, 0x04, 0x93 };
DeviceAddress heater_sensor = { 0x28, 0xFF, 0x98, 0x8B, 0x33, 0x17, 0x04, 0x55 };

OneWire oneWire(ALLSKY_PINOUT_DALLAS_BUS);
DallasTemperature sensors(&oneWire);

void allsky_dallas_init(void){
    sensors.begin();

    if(sensors.getDeviceCount() != 3){
        Serial.println("ERROR=>Not all temp sensors are connected");
    }

    oneWire.reset_search();
    if (!oneWire.search(dome_sensor))   Serial.println("ERROR=>Unable to find address for dome sensor" );
    if (!oneWire.search(air_sensor))    Serial.println("ERROR=>Unable to find address for air tank"    );
    if (!oneWire.search(heater_sensor)) Serial.println("ERROR=>Unable to find address for heating tank");

    sensors.setResolution(dome_sensor,   ALLSKY_DALLAS_TEMPERATURE_PRECISION);
    sensors.setResolution(air_sensor,    ALLSKY_DALLAS_TEMPERATURE_PRECISION);
    sensors.setResolution(heater_sensor, ALLSKY_DALLAS_TEMPERATURE_PRECISION);
}

void allsky_dallas_update(void){
    sensors.requestTemperatures();
    allsky_dallas_temp_dome    = sensors.getTempC(dome_sensor);
    allsky_dallas_temp_air     = sensors.getTempC(air_sensor);
    allsky_dallas_temp_heating = sensors.getTempC(heater_sensor);
}
