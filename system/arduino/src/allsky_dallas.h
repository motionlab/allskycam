#ifndef ALLSKY_DALLAS_H
#define ALLSKY_DALLAS_H

#define ALLSKY_DALLAS_TEMPERATURE_PRECISION 12

extern float allsky_dallas_temp_air;
extern float allsky_dallas_temp_dome;
extern float allsky_dallas_temp_heating;

void allsky_dallas_init  (void);
void allsky_dallas_update(void);

#endif
