#ifndef ALLSKY_HEATING_H
#define ALLSKY_HEATING_H

extern int8_t allsky_heating_temperature;
extern bool   allsky_heating_state;

#define ALLSKY_HEATING_ON        0
#define ALLSKY_HEATING_OFF       1
#define ALLSKY_HEATING_HYSTERESE 2

void allsky_heating_init   (void);
void allsky_heating_process(void);

#endif
