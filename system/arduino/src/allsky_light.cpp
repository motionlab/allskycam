#include <Arduino.h>
#include "allsky_light.h"
#include "allsky_pinout.h"
#include <Wire.h>
#include "Digital_Light_TSL2561.h"

uint32_t allsky_light_visible = 0;
float    allsky_light_uv      = 0;

void allsky_light_init(void){
    Wire.begin();
    TSL2561.init();
    pinMode(ALLSKY_PINOUT_LIGHT_UV, INPUT);
}

void allsky_light_update(void){
    allsky_light_visible = TSL2561.readVisibleLux();
    double temp = 0;

    for(uint8_t i = 0; i < ALLSKY_LIGHT_SAMPLE_SIZE; i++){
        temp += analogRead(ALLSKY_PINOUT_LIGHT_UV);
    }
    allsky_light_uv = temp / ALLSKY_LIGHT_SAMPLE_SIZE;
}
