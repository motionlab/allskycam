#ifndef ALLSKY_UPLINK_H
#define ALLSKY_UPLINK_H

#define ALLSKY_UPLINK_RECEIVE_BUFFER_SIZE 10

#define ALLSKY_UPLINK_MIN_TEMP  0
#define ALLSKY_UPLINK_MAX_TEMP 42

void allsky_uplink_init   (void);
void allsky_uplink_send   (void);
void allsky_uplink_receive(void);

#endif
