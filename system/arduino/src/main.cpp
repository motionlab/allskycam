#include <Arduino.h>
#include "main.h"
#include "allsky_dallas.h"
#include "allsky_uplink.h"
#include "allsky_pinout.h"
#include "allsky_light.h"
#include "allsky_sound.h"
#include "allsky_heating.h"
#include "allsky_motion.h"

uint32_t allsky_main_scheduler_counter_1_sec    = 0;
uint32_t allsky_main_scheduler_counter_100mssec = 0;

void allsky_main_execute_jobs_1s   (void);
void allsky_main_execute_jobs_100ms(void);
void allsky_main_execute_jobs_fast (void);
void allsky_main_blink             (void);

void setup(void){

    Serial.begin(9600);
    while (!Serial) {}
    Serial.println("INFO=>Allsky Thermo Nuclear Unit");

    pinMode(LED_BUILTIN, OUTPUT);

    allsky_dallas_init();
    allsky_uplink_init();
    allsky_light_init();
    allsky_sound_init();
    allsky_heating_init();
    allsky_motion_init();
}

void loop(void){

    if (allsky_main_scheduler_counter_1_sec > millis()){
        allsky_main_scheduler_counter_1_sec = 0;
    }

    if (allsky_main_scheduler_counter_100mssec > millis()){
        allsky_main_scheduler_counter_100mssec = 0;
    }

    if ((allsky_main_scheduler_counter_1_sec + 1000) < millis()){
        allsky_main_scheduler_counter_1_sec = millis();
        allsky_main_execute_jobs_1s();
    }

    if ((allsky_main_scheduler_counter_100mssec + 100) < millis()){
        allsky_main_scheduler_counter_100mssec = millis();
        allsky_main_execute_jobs_100ms();
    }
    allsky_main_execute_jobs_fast();
}


void allsky_main_execute_jobs_1s(void){
    allsky_main_blink();
    allsky_dallas_update();
    allsky_heating_process();
    allsky_motion_update();
    allsky_light_update();
    allsky_uplink_send();
}

void allsky_main_execute_jobs_100ms(void){
    allsky_uplink_receive();
}

void allsky_main_execute_jobs_fast(void){
    allsky_sound_update();
}

void allsky_main_blink(void){
    bool val = digitalRead(LED_BUILTIN);
    digitalWrite(LED_BUILTIN, !val);
}
