#include <Arduino.h>
#include "allsky_heating.h"
#include "allsky_pinout.h"
#include "allsky_dallas.h"

int8_t allsky_heating_temperature = 10;
bool   allsky_heating_state       = false;

void allsky_heating_init(void){
    pinMode(ALLSKY_PINOUT_HEATING, OUTPUT);
    digitalWrite(ALLSKY_PINOUT_HEATING, ALLSKY_HEATING_OFF);
}

void allsky_heating_process(void){

    if(allsky_heating_state && allsky_dallas_temp_dome > (allsky_heating_temperature + ALLSKY_HEATING_HYSTERESE)){
        digitalWrite(ALLSKY_PINOUT_HEATING, ALLSKY_HEATING_OFF);
        allsky_heating_state = false;
    }

    if(!allsky_heating_state && allsky_dallas_temp_dome < (allsky_heating_temperature - ALLSKY_HEATING_HYSTERESE)){
        digitalWrite(ALLSKY_PINOUT_HEATING, ALLSKY_HEATING_ON);
        allsky_heating_state = true;
    }

}
