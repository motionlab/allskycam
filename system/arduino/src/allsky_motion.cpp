#include <Arduino.h>
#include "allsky_motion.h"
#include "allsky_pinout.h"

bool allsky_motion_state = false;

void allsky_motion_init(void){
    pinMode(ALLSKY_PINOUT_MOTION, INPUT);
}

void allsky_motion_update(void){
    allsky_motion_state = digitalRead(ALLSKY_PINOUT_MOTION);
}
