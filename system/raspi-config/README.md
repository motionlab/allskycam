# USB rulez

## Install
```bash
sudo cp 50-allsky.rules /etc/udev/rules.d/
```

## Discover
```
udevadm info -a -p  $(udevadm info -q path -n /dev/ttyUSB0)
```

# Service
## Install
```
for i in arduino
do
 sudo cp allsky-${i}.service /lib/systemd/system/allsky-${i}.service
 sudo chmod 644              /lib/systemd/system/allsky-${i}.service
 sudo systemctl daemon-reload
 sudo systemctl enable allsky-${i}.service
 sudo systemctl start  allsky-${i}.service
done
```

## Check
https://wiki.archlinux.org/title/systemd
```
sudo systemctl status allsky-arduino.service
sudo systemctl start  allsky-arduino.service
sudo systemctl stop   allsky-arduino.service
sudo journalctl -f -u allsky-arduino.service
```
