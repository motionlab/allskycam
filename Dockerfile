FROM python:latest
ARG  DEBIAN_FRONTEND=noninteractive

RUN  useradd -r -u 1001 appuser

RUN apt update
RUN apt upgrade -y
RUN apt install -y mc vim ffmpeg git gawk imagemagick

ADD  requirements.txt /requirements.txt
RUN  pip install --no-cache-dir -r /requirements.txt

RUN mkdir /app
RUN chown appuser /app

USER appuser

ADD  templates   /app/templates
ADD  generate.py /app/generate.py

ADD timelapse.sh      /app/timelapse.sh
ADD movedaypicture.sh /app/movedaypicture.sh

WORKDIR /app
