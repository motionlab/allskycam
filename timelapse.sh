#!/bin/bash

cd /host

for d in `find /host/ -maxdepth 1 -type d | grep 20 | sort` # | sed '$d'`
do

  echo "Processing ${d}"
  cd ${d}

  # Check for timelapse
  count=`ls -1 *.mp4 2>/dev/null | wc -l`
  if [ $count == 0 ]
  then
   echo "No timelapse found, generating"

   # Cleanup  
   find ./ -name "*.jpg" -size 0 -delete
   rm thumbnails/*

   # Timelapse
   mkdir sequence/
   ls -rt *.jpg | gawk 'BEGIN{ a=1 }{ printf "cp %s sequence/%04d.jpg\n", $0, a++ }' | bash
   ffmpeg -y -f image2 \
    -r 23 \
    -i sequence/%04d.jpg \
    -vcodec libx264 \
    -b:v 2000k \
    -pix_fmt yuv420p \
    -movflags +faststart \
    ${d}/timelapse.mp4
   rm -rf sequence
   
   mkdir original
   for i in `ls image*.jpg`
   do
    convert -thumbnail 1% ${i} thumbnails/${i}
    mv ${i} original/
   done   

   # Keogram
   mv keogram/*.jpg keogram.jpg
   rm -rf keogram
   
   # Startrails
   mv startrails/*.jpg startrails.jpg 
   rm -rf startrails

   cd ..
  fi
done
